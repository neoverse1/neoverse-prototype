using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_Buttons : MonoBehaviour
{
    
    public Chessboard cs;
    private void Start()
    {
        cs = FindObjectOfType<Chessboard>();
    }

    public void OnResetButton()
    {
        cs.victoryScreen.transform.GetChild(0).gameObject.SetActive(false);
        cs.victoryScreen.transform.GetChild(1).gameObject.SetActive(false);
        cs.victoryScreen.SetActive(false);
        
        //Clean up
        cs.Reset();
        
    }

    public void OnExitButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("ChessClub");
    }
}
