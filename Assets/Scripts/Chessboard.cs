using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
public class Chessboard : MonoBehaviourPunCallbacks, IOnEventCallback
{
    //ART
    [Header("Art Stuff:")]
    [SerializeField] private Material tileMaterial;
    [SerializeField] private float tileSize = 1.0f;
    [SerializeField] private float yOffSet = 0.2f;
    [SerializeField] private Vector3 boardCenter = Vector3.zero;
    [SerializeField] private float deadSize = 3.0f;
    [SerializeField] private float deadSpacing = 0.3f;
    [SerializeField] private float dragOffset = 0.85f;

    public GameObject victoryScreen;
    public GameObject waitingScreen;

    //PREFABS
    [Header("Prefabs & Materials:")]
    [SerializeField] private GameObject[] prefabs;
    [SerializeField] private Material[] teamMaterials;

    //Logic
    private ChessPieces[,] chessPiece;
    private ChessPieces currentDragging;
    private const int TILE_COUNT_X = 8;
    private const int TILE_COUNT_y = 8;
    private GameObject[,] tiles;
    private Camera currentCamera;
    private Vector2Int currentHover;
    private Vector3 bounds;
    private List<Vector2Int> availableMoves = new List<Vector2Int>();
    private List<ChessPieces> deadWhites = new List<ChessPieces>();
    private List<ChessPieces> deadBlacks = new List<ChessPieces>();
    private bool isWhiteTurn;
    public int teamID = 0;

    public Camera cameraWhite;
    public Camera cameraBlack;

    //XR Input System
    //Getting Instance of the  Hand Devices
    private UnityEngine.XR.InputDevice rightHandDevice;
    private UnityEngine.XR.InputDevice leftHandDevice;
    private bool triggerValue;
    private bool canStart = false;

    //Photon Networking Vars
    public const byte MoveEventCode = 1;


    //Unity Functions
    private void Awake()
    {
        isWhiteTurn = true;
        rightHandDevice = UnityEngine.XR.InputDevices.GetDeviceAtXRNode(UnityEngine.XR.XRNode.RightHand);
        leftHandDevice = UnityEngine.XR.InputDevices.GetDeviceAtXRNode(UnityEngine.XR.XRNode.LeftHand);

        GenerateAllTiles(tileSize, 8, 8);
        SpawnAllPieces();
        PositionAllPieces();

    }
    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            teamID = 0;
            cameraBlack.gameObject.SetActive(false);
            cameraWhite.gameObject.SetActive(true);
        }
        else
        {
            teamID = 1;
            cameraWhite.gameObject.SetActive(false);
            cameraBlack.gameObject.SetActive(true);
        }
        if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
        {

            photonView.RPC("ChangeState", RpcTarget.All);
        }

        // PhotonNetwork.AutomaticallySyncScene = true;
    }
    [PunRPC]
    void ChangeState()
    {
        canStart = true;
        waitingScreen.SetActive(false);
    }

    private void Update()
    {
        if (!canStart)
        {
            waitingScreen.SetActive(true);
        }
        else
        {

            if (!currentCamera)
            {
                currentCamera = Camera.main;
                return;
            }

            RaycastHit info;
            Ray ray = currentCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out info, 100, LayerMask.GetMask("Tile", "Hover", "Highlight")))
            {

                Vector2Int hitPosition = LookUpTileIndex(info.transform.gameObject);

                if (currentHover == -Vector2Int.one)
                {
                    //Hovering over a tile after not hovering any tiles:
                    currentHover = hitPosition;
                    tiles[hitPosition.x, hitPosition.y].layer = LayerMask.NameToLayer("Hover");
                }
                //If we were already hovering a tile, change the previous one.
                if (currentHover != hitPosition)
                {
                    tiles[currentHover.x, currentHover.y].layer = (containsValidMove(ref availableMoves, currentHover)) ? LayerMask.NameToLayer("Highlight") : LayerMask.NameToLayer("Tile");
                    currentHover = hitPosition;
                    tiles[hitPosition.x, hitPosition.y].layer = LayerMask.NameToLayer("Hover");
                }
                //Mouse Click event
                rightHandDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out triggerValue); //getting the value of right hand trigger
                if (Input.GetMouseButtonDown(0))
                {
                    if (chessPiece[hitPosition.x, hitPosition.y] != null)
                    {
                        // Is it our turn
                        if ((chessPiece[hitPosition.x, hitPosition.y].team == 0 && isWhiteTurn) && teamID == 0)
                        {
                            currentDragging = chessPiece[hitPosition.x, hitPosition.y];
                            //Get a list of where the selected piece can go, highlight the tile as well
                            availableMoves = currentDragging.GetAvailableMoves(ref chessPiece, TILE_COUNT_X, TILE_COUNT_y);
                            HighlightTiles();
                        }
                        if (chessPiece[hitPosition.x, hitPosition.y].team == 1 && !isWhiteTurn && teamID == 1)
                        {
                            currentDragging = chessPiece[hitPosition.x, hitPosition.y];
                            //Get a list of where the selected piece can go, highlight the tile as well
                            availableMoves = currentDragging.GetAvailableMoves(ref chessPiece, TILE_COUNT_X, TILE_COUNT_y);
                            HighlightTiles();
                        }

                    }
                }
                //Releasing the mouse button
                if (currentDragging != null && Input.GetMouseButtonUp(0))
                {
                    Vector2Int previousPosition = new Vector2Int(currentDragging.currentX, currentDragging.currentY);

                    if (containsValidMove(ref availableMoves, new Vector2(hitPosition.x, hitPosition.y)))
                    {
                        MoveTo(previousPosition.x, previousPosition.y, hitPosition.x, hitPosition.y);

                        //event code
                        object[] moveData = new object[] { previousPosition.x, previousPosition.y, hitPosition.x, hitPosition.y, teamID };
                        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
                        PhotonNetwork.RaiseEvent(MoveEventCode, moveData, raiseEventOptions, ExitGames.Client.Photon.SendOptions.SendReliable);
                    }
                    else
                    {
                        currentDragging.SetPosition(GetTileCenter(previousPosition.x, previousPosition.y));
                        currentDragging = null;
                        RemoveHighlightTiles();
                    }


                }
            }
            else
            {
                if (currentHover != -Vector2Int.one)
                {
                    tiles[currentHover.x, currentHover.y].layer = (containsValidMove(ref availableMoves, currentHover)) ? LayerMask.NameToLayer("Highlight") : LayerMask.NameToLayer("Tile");
                    currentHover = -Vector2Int.one;
                }

                if (currentDragging && Input.GetMouseButtonUp(0))
                {
                    currentDragging.SetPosition(GetTileCenter(currentDragging.currentX, currentDragging.currentY));

                }
                currentDragging = null;
                RemoveHighlightTiles();
            }
            //If we're a dragging a piece
            if (currentDragging)
            {
                Plane horizontalPlane = new Plane(Vector3.up, Vector3.up * yOffSet);
                float distance = 0.0f;
                if (horizontalPlane.Raycast(ray, out distance))
                    currentDragging.SetPosition(ray.GetPoint(distance) + Vector3.up * dragOffset);


            }
        }
    }


    //Generate the Grid
    private void GenerateAllTiles(float tileSize, int tileCountX, int tileCountY)
    {
        yOffSet += transform.position.y;
        bounds = new Vector3((tileCountX / 2) * tileSize, 0, (tileCountX / 2) * tileSize) + boardCenter;

        tiles = new GameObject[tileCountX, tileCountY];
        for (int x = 0; x < tileCountX; x++)
            for (int y = 0; y < tileCountY; y++)
                tiles[x, y] = GenerateSingleTile(tileSize, x, y);
    }
    private GameObject GenerateSingleTile(float tileSize, int x, int y)
    {
        GameObject tileObj = new GameObject(String.Format("X:{0}, Y:{1}", x, y));
        tileObj.transform.parent = transform;

        Mesh mesh = new Mesh();
        tileObj.AddComponent<MeshFilter>().mesh = mesh;
        tileObj.AddComponent<MeshRenderer>().material = tileMaterial;

        //Generating Geometry 
        Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(x * tileSize, yOffSet, y * tileSize) - bounds;
        vertices[1] = new Vector3(x * tileSize, yOffSet, (y + 1) * tileSize) - bounds;
        vertices[2] = new Vector3((x + 1) * tileSize, yOffSet, y * tileSize) - bounds;
        vertices[3] = new Vector3((x + 1) * tileSize, yOffSet, (y + 1) * tileSize) - bounds;

        int[] tris = new int[] { 0, 1, 2, 1, 3, 2 };
        mesh.vertices = vertices;
        mesh.triangles = tris;
        mesh.RecalculateNormals();
        tileObj.layer = LayerMask.NameToLayer("Tile");
        tileObj.AddComponent<BoxCollider>();

        return tileObj;
    }


    //Spawning of pieces

    private void SpawnAllPieces()
    {
        chessPiece = new ChessPieces[TILE_COUNT_X, TILE_COUNT_y];

        int whiteTeam = 0, blackTeam = 1;

        //White Team
        chessPiece[0, 0] = SpawnSinglePiece(ChessPieceType.Rook, whiteTeam);
        chessPiece[1, 0] = SpawnSinglePiece(ChessPieceType.Knight, whiteTeam);
        chessPiece[2, 0] = SpawnSinglePiece(ChessPieceType.Bishop, whiteTeam);
        chessPiece[3, 0] = SpawnSinglePiece(ChessPieceType.Queen, whiteTeam);
        chessPiece[4, 0] = SpawnSinglePiece(ChessPieceType.King, whiteTeam);
        chessPiece[5, 0] = SpawnSinglePiece(ChessPieceType.Bishop, whiteTeam);
        chessPiece[6, 0] = SpawnSinglePiece(ChessPieceType.Knight, whiteTeam);
        chessPiece[7, 0] = SpawnSinglePiece(ChessPieceType.Rook, whiteTeam);

        for (int i = 0; i < TILE_COUNT_X; i++)
            chessPiece[i, 1] = SpawnSinglePiece(ChessPieceType.Pawn, whiteTeam);

        //Black Team
        chessPiece[0, 7] = SpawnSinglePiece(ChessPieceType.Rook, blackTeam);
        chessPiece[1, 7] = SpawnSinglePiece(ChessPieceType.Knight, blackTeam);
        chessPiece[2, 7] = SpawnSinglePiece(ChessPieceType.Bishop, blackTeam);
        chessPiece[3, 7] = SpawnSinglePiece(ChessPieceType.Queen, blackTeam);
        chessPiece[4, 7] = SpawnSinglePiece(ChessPieceType.King, blackTeam);
        chessPiece[5, 7] = SpawnSinglePiece(ChessPieceType.Bishop, blackTeam);
        chessPiece[6, 7] = SpawnSinglePiece(ChessPieceType.Knight, blackTeam);
        chessPiece[7, 7] = SpawnSinglePiece(ChessPieceType.Rook, blackTeam);

        for (int i = 0; i < TILE_COUNT_X; i++)
            chessPiece[i, 6] = SpawnSinglePiece(ChessPieceType.Pawn, blackTeam);


    }

    /* photon network spawning
    private void SpawnAllPieces()
    {
        chessPiece = new ChessPieces[TILE_COUNT_X, TILE_COUNT_y];

        int whiteTeam = 0, blackTeam = 1;
        int parentViewID = this.gameObject.GetComponent<PhotonView>().ViewID;
        object[] initData = new object[5];
        initData[0] = parentViewID;
       if(PhotonNetwork.IsMasterClient)
        {
            chessPiece[0, 0] = SpawnWhitePiece("Rook_W", whiteTeam, ChessPieceType.Rook,initData, 0,0);
            chessPiece[1, 0] = SpawnWhitePiece("Knight_W", whiteTeam, ChessPieceType.Knight, initData,1,0);
            chessPiece[2, 0] = SpawnWhitePiece("Bishop_W", whiteTeam, ChessPieceType.Bishop, initData,2,0);
            chessPiece[3, 0] = SpawnWhitePiece("Queen_W", whiteTeam, ChessPieceType.Queen, initData,3,0);
            chessPiece[4, 0] = SpawnWhitePiece("King_W", whiteTeam, ChessPieceType.King, initData,4,0);
            chessPiece[5, 0] = SpawnWhitePiece("Bishop_W", whiteTeam, ChessPieceType.Bishop, initData,5,0);
            chessPiece[6, 0] = SpawnWhitePiece("Knight_W", whiteTeam, ChessPieceType.Knight, initData,6,0);
            chessPiece[7, 0] = SpawnWhitePiece("Rook_W", whiteTeam, ChessPieceType.Rook, initData,7,0);

            for (int i = 0; i < TILE_COUNT_X; i++)
                chessPiece[i, 1] = SpawnWhitePiece("Pawn_W", whiteTeam, ChessPieceType.Pawn, initData,i,1);

        }
       else
        {
            chessPiece[0, 7] = SpawnBlackPiece("Rook", blackTeam, ChessPieceType.Rook, initData,0,7);
            chessPiece[1, 7] = SpawnBlackPiece("Knight", blackTeam, ChessPieceType.Knight, initData,1,7);
            chessPiece[2, 7] = SpawnBlackPiece("Bishop", blackTeam, ChessPieceType.Bishop, initData, 2, 7);
            chessPiece[3, 7] = SpawnBlackPiece("Queen", blackTeam, ChessPieceType.Queen, initData, 3, 7);
            chessPiece[4, 7] = SpawnBlackPiece("King", blackTeam, ChessPieceType.King, initData, 4, 7);
            chessPiece[5, 7] = SpawnBlackPiece("Bishop", blackTeam, ChessPieceType.Bishop, initData, 5, 7);
            chessPiece[6, 7] = SpawnBlackPiece("Knight", blackTeam, ChessPieceType.Knight, initData, 6, 7);
            chessPiece[7, 7] = SpawnBlackPiece("Rook", blackTeam, ChessPieceType.Rook, initData, 7, 7);

            for (int i = 0; i < TILE_COUNT_X; i++)
                chessPiece[i, 6] = SpawnBlackPiece("Pawn", blackTeam, ChessPieceType.Pawn, initData, i, 6);
       }
    }
    */

    private ChessPieces SpawnSinglePiece(ChessPieceType type, int team)
    {
        ChessPieces cp = Instantiate(prefabs[(int)type - 1], transform).GetComponent<ChessPieces>();
        //ChessPieces cp = PhotonNetwork.Instantiate(prefabs[(int)type - 1].name, transform.position, transform.rotation).GetComponent<ChessPieces>();
        cp.type = type;
        cp.team = team;
        cp.GetComponent<MeshRenderer>().material = teamMaterials[team];

        return cp;
    }


    ///Photon Spawning
    /*private ChessPieces SpawnBlackPiece(String pieceName, int team, ChessPieceType type, object[] data, int x, int y)
    {
        data[1] = type;
        data[2] = team;
        data[3] = x;
        data[4] = y;
        ChessPieces cp = PhotonNetwork.Instantiate(pieceName, transform.position, transform.rotation, 0, data).GetComponent<ChessPieces>();
        //ChessPieces rcp = PhotonNetwork.InstantiateRoomObject(pieceName, transform.position, transform.rotation).GetComponent<ChessPieces>();
        cp.team = team;
        cp.type = type;
        return cp;
    }
    private ChessPieces SpawnWhitePiece(String pieceName, int team, ChessPieceType type, object[] data, int x, int y)
    {
        data[1] = type;
        data[2] = team;
        data[3] = x;
        data[4] = y;
        ChessPieces cp = PhotonNetwork.Instantiate(pieceName, transform.position, transform.rotation, 0 ,data).GetComponent<ChessPieces>();
        //ChessPieces cp = PhotonNetwork.InstantiateRoomObject(pieceName, transform.position, transform.rotation).GetComponent<ChessPieces>();

        cp.team = team;
        cp.type = type;
        return cp;
    }
    */

    //Positioning of all the pieces on the board

    private void PositionAllPieces()
    {
        for (int x = 0; x < TILE_COUNT_X; x++)
            for (int y = 0; y < TILE_COUNT_y; y++)
                if (chessPiece[x, y] != null)

                    PositionSinglePiece(x, y, true);

    }

    void PositionSinglePiece(int x, int y, bool force = false)
    {
        chessPiece[x, y].currentX = x;
        chessPiece[x, y].currentY = y;
        chessPiece[x, y].SetPosition(GetTileCenter(x, y), force);

    }


    // Highlight Tiles
    private void HighlightTiles()
    {
        for (int i = 0; i < availableMoves.Count; i++)
            tiles[availableMoves[i].x, availableMoves[i].y].layer = LayerMask.NameToLayer("Highlight");
    }
    private void RemoveHighlightTiles()
    {
        for (int i = 0; i < availableMoves.Count; i++)
            tiles[availableMoves[i].x, availableMoves[i].y].layer = LayerMask.NameToLayer("Tile");

        availableMoves.Clear();
    }
    private Vector3 GetTileCenter(int x, int y)
    {
        return new Vector3(x * tileSize, yOffSet, y * tileSize) - bounds + new Vector3(tileSize / 2, 0, tileSize / 2);
    }

    //Check Mate
    public void CheckMate(int team)
    {
        DisplayVictory(team);
    }

    public void DisplayVictory(int winningTeam)
    {
        victoryScreen.SetActive(true);
        victoryScreen.transform.GetChild(winningTeam).gameObject.SetActive(true);
    }

    public void Reset()
    {/*
        //Clean Up - This is Part - 2 of the "OnResetButton()" Method from the Class: "UI_Buttons"

        //Resetting fields
        currentDragging = null;
        availableMoves = new List<Vector2Int>();
        //Cleaning Up
        for (int x = 0; x < TILE_COUNT_X; x++)
        {
            for (int y = 0; y < TILE_COUNT_y; y++)
            {
                if (chessPiece[x, y] != null)
                    Destroy(chessPiece[x, y].gameObject);
                chessPiece[x, y] = null;
            }
        }

        for (int i = 0; i < deadWhites.Count; i++)
            Destroy(deadWhites[i].gameObject);
        for (int i = 0; i < deadBlacks.Count; i++)
            Destroy(deadBlacks[i].gameObject);
        deadWhites.Clear();
        deadBlacks.Clear();

        SpawnAllPieces();
        PositionAllPieces();
        isWhiteTurn = true;*/
    }

    //Operations
    private bool containsValidMove(ref List<Vector2Int> moves, Vector2 pos)
    {
        for (int i = 0; i < moves.Count; i++)
            if (moves[i].x == pos.x && moves[i].y == pos.y)
                return true;
        return false;


    }
    private Vector2Int LookUpTileIndex(GameObject hitinfo)
    {
        for (int x = 0; x < TILE_COUNT_X; x++)
            for (int y = 0; y < TILE_COUNT_y; y++)
                if (tiles[x, y] == hitinfo)
                    return new Vector2Int(x, y);
        return -Vector2Int.one;

    }
    private void MoveTo(int originalX, int originalY, int x, int y)
    {


        ChessPieces cp = chessPiece[originalX, originalY];
        Vector2Int previousPosition = new Vector2Int(originalX, originalY);

        // Is there another piece on the target position
        if (chessPiece[x, y] != null)
        {
            ChessPieces ocp = chessPiece[x, y];
            if (cp.team == ocp.team)
                return;
            
            if (ocp.team == 0)
            {
                if (ocp.type == ChessPieceType.King)
                {
                    CheckMate(1);
                }

                deadWhites.Add(ocp);
                ocp.SetScale(Vector3.one * deadSize);
                ocp.SetPosition(new Vector3(8 * tileSize, yOffSet, -1 * tileSize) - bounds + new Vector3(tileSize / 2, 0, tileSize / 2) + (Vector3.forward * deadSpacing) * deadWhites.Count);
            }
            else
            {
                if (ocp.type == ChessPieceType.King)
                {
                    CheckMate(0);
                }
                deadBlacks.Add(ocp);
                ocp.SetScale(Vector3.one * deadSize);
                ocp.SetPosition(new Vector3(-1 * tileSize, yOffSet, 8 * tileSize) - bounds + new Vector3(tileSize / 2, 0, tileSize / 2) + (Vector3.back * deadSpacing) * deadBlacks.Count);
            }
        }
        chessPiece[x, y] = cp;
        chessPiece[previousPosition.x, previousPosition.y] = null;

        PositionSinglePiece(x, y);

        isWhiteTurn = !isWhiteTurn;

        if (currentDragging)
            currentDragging = null;
        RemoveHighlightTiles();
        //photonView.RPC("changeTurn", RpcTarget.All, isWhiteTurn);

        return;
    }

    [PunRPC]
    void changeTurn(bool val)
    {
        this.isWhiteTurn = val;
    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;
        if (eventCode == MoveEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            if (teamID != (int)data[4])
            {
                ChessPieces target = chessPiece[(int)data[0], (int)data[1]];

                availableMoves = target.GetAvailableMoves(ref chessPiece, TILE_COUNT_X, TILE_COUNT_y);
                MoveTo((int)data[0], (int)data[1], (int)data[2], (int)data[3]);
            }

        }
        //throw new NotImplementedException();
    }
}
