using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class fpsCounter : MonoBehaviour
{
    public TMP_Text fpsDisplay;// = GetComponent<TextMeshProUGUI>();
    // Start is called before the first frame update
    IEnumerator Start()
    {
        while(true)
        {
            yield return new WaitForSeconds(0.1f);
            string toString = (1f / Time.deltaTime).ToString();
            fpsDisplay.text = toString;
            yield return new WaitForSeconds(0.5f);
        }
    }

    // Update is called once per frame
    void Update()
    {

        
    }
}
