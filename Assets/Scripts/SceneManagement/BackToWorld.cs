using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToWorld : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag("Player"))
        {
           
            SceneManager.LoadScene("Kingdom_QuarterSize");
        }
    }
    
}
