using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class ClubNetworkManager : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private ChessQuestBoard cqb;
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        if(cqb.joinRoom)
        {
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = 10;
            roomOptions.IsVisible = false;
            roomOptions.IsOpen = true;

            PhotonNetwork.JoinOrCreateRoom("ChessClub", roomOptions, TypedLobby.Default);
        }

       
    }
}
