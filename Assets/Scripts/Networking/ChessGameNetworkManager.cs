using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class ChessGameNetworkManager : MonoBehaviourPunCallbacks
{
    private string[] properties;

    
    void Start()
    {
        CreateOrJoinRoom();
        
    }

 
   
    private void CreateOrJoinRoom()
    {
        if(SceneDataPasser.roomName != null)
        {
            PhotonNetwork.JoinRoom(SceneDataPasser.roomName);
            SceneDataPasser.roomName = null;
        }
        else
        {
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = 2;
            roomOptions.IsVisible = !SceneDataPasser.isPrivateGame;
            roomOptions.IsOpen = true;
            ExitGames.Client.Photon.Hashtable custRoomProps = new ExitGames.Client.Photon.Hashtable();
            custRoomProps.Add("w", SceneDataPasser.WagerAmount);
            roomOptions.CustomRoomProperties = custRoomProps;
            properties = new string[] { "w" };
            roomOptions.CustomRoomPropertiesForLobby = properties;
            //PhotonNetwork.CreateRoom("Chess Game Room", roomOptions, TypedLobby.Default);
            PhotonNetwork.JoinOrCreateRoom("ChessGameRoom", roomOptions, TypedLobby.Default);
        }
       

    }
    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();

        PhotonNetwork.CurrentRoom.SetPropertiesListedInLobby(properties);

    }
    public void QuitMatchmaking()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel("ChessClub");
    }
}
