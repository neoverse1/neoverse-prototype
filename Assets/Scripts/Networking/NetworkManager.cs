using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
public class NetworkManager : MonoBehaviourPunCallbacks
{
    //Script to intialize and establish connection to the photon servers and then connect the player to a room.

    [SerializeField]
    private TMP_Text connectionStatus;

    [SerializeField]
    private GameObject waitingScreenCanvas;

    [SerializeField]
    private GameObject movementScreenCanvas;


    void Start()
    {
        
        ConnectToServer();

    }

   

    void ConnectToServer()
    {
        PhotonNetwork.ConnectUsingSettings();
        connectionStatus.text = "Connecting to server";
        
    }

    public override void OnConnectedToMaster()
    {
        connectionStatus.text = "Connected to server, Trying to join a room";
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 5;
        roomOptions.IsVisible = true;
        roomOptions.IsOpen = true;

        PhotonNetwork.JoinOrCreateRoom("Main Hub", roomOptions, TypedLobby.Default);

    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        waitingScreenCanvas.SetActive(false);
        movementScreenCanvas.SetActive(true);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("new player entered room");
    }

    
}
