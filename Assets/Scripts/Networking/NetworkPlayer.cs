using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class NetworkPlayer : MonoBehaviour, IPunInstantiateMagicCallback
{
    //Currently useless script but can be updated with player specific network functions 

    private PhotonView photonview;

    private GameObject Skin;

    private int skinIndex;
    

    void Start()
    {
       photonview = GetComponent<PhotonView>();
        if(photonview.IsMine)
        {
            transform.gameObject.tag = "Player";
        }

        
    }



    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        object[] instantiationData = info.photonView.InstantiationData;
        skinIndex = (int)instantiationData[0];
        Skin = this.transform.GetChild(skinIndex).gameObject;
        Skin.SetActive(true);

    }
}
