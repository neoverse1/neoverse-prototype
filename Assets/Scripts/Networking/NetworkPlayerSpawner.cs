using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class NetworkPlayerSpawner :MonoBehaviourPunCallbacks
{
    //Script to spawn players over the network

    private GameObject spawnedPlayerPrefab;     //Prefab of the spawned player, found in assets>resources

    private PhotonView mPhotonView;

    //Some components needed to be set up on the player for the unity standard third person controller asset to work 

    [SerializeField]
    private Cinemachine.CinemachineVirtualCamera cinemachine;

    [SerializeField]
    private StarterAssets.UICanvasControllerInput uICanvasControllerInput;

    [SerializeField]
    private MobileDisableAutoSwitchControls mobileDisableAutoSwitchControls;

    private GameObject Skin;

   
 //2 23
    public override void OnJoinedRoom()
    {
        Debug.Log("Joined a room, called from network player spawner");
        base.OnJoinedRoom();
        if (SceneDataPasser.skinIndex == 0)
        { SceneDataPasser.skinIndex = Random.Range(2, 24); }

        object[] customInitData = new object[]
        {
            SceneDataPasser.skinIndex
        };
        
        spawnedPlayerPrefab = PhotonNetwork.Instantiate("Network Player Diff", transform.position, transform.rotation,0,customInitData); //a player prefab gets spawned for everyone in the room whenever someone joins a room
        
        mPhotonView = spawnedPlayerPrefab.GetPhotonView();
        if (mPhotonView.IsMine)    //so the camera doesn't know which one of the player prefab is us, therfore checking through photon view if the prefab is ours and then setting it as the camera follow target
        {
            
            cinemachine.Follow = spawnedPlayerPrefab.transform.GetChild(0);
            
            uICanvasControllerInput.starterAssetsInputs = spawnedPlayerPrefab.GetComponent<StarterAssets.StarterAssetsInputs>();
            mobileDisableAutoSwitchControls.playerInput = spawnedPlayerPrefab.GetComponent<UnityEngine.InputSystem.PlayerInput>();

        }
    }

   
    
    public override void OnLeftRoom()
    {
        
        base.OnLeftRoom();
        PhotonNetwork.Destroy(spawnedPlayerPrefab);   //Destroy the player prefab when they leave the room
    }

    

}
