using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoralisUIManager : MonoBehaviour
{
    public GameObject moralisUI;
    public GameObject mainUI;
    public void onButtonClick()
    {    
        mainUI.SetActive(false);
        moralisUI.SetActive(true);
    }
    public void onCloseButttonClick()
    {
        moralisUI.SetActive(false);
        mainUI.SetActive(true);
    }
}
