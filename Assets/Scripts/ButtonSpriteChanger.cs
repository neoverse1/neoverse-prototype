using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonSpriteChanger : MonoBehaviour
{
    [SerializeField]
    private Sprite onSprite;                 //Sprite icon for mic enabled

    [SerializeField]
    private Sprite offSprite;               //Sprite icon for mic disabled

    [SerializeField]
    private UnityEngine.UI.Button button;

    [SerializeField]
    private GameObject recorder;            //Photon voice recorder component, named as Network Voice in heirarchy 


    public void ChangeImage()
    {
        GameObject icon = button.transform.GetChild(0).gameObject;   //Get the icon of the button
       
        if (recorder.activeInHierarchy)
        {
            icon.GetComponent<Image>().sprite = offSprite;      
            recorder.SetActive(false);                              //Disabling voice transmission when the mic is muted
        }
        else
        {
            icon.GetComponent<Image>().sprite = onSprite;
            recorder.SetActive(true);
        }
    }
}
