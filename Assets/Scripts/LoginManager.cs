using System;
using MoralisUnity;
// using MoralisUnity.Kits.AuthenticationKit;
// using MoralisUnity.Platform;
// using MoralisUnity.Platform.Objects;
// using MoralisUnity.Web3Api;
// using MoralisUnity.Sdk;
// using Org.BouncyCastle.Asn1.Cms;
using UnityEngine;
using Photon.Pun;
using TMPro;
public class LoginManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private TMP_InputField playerUserName, playerPassword;
    #region Unity Methods

    void Start()
    {
    }
    void Update()
    {
    }
    #endregion
    #region UI Callback Methods
    
    public async void Signin()
    {
        try
        {
            Moralis.GetClient();
            Debug.Log("Client Initialized in GetClient()");
        }
        catch (Exception)
        {
            Moralis.Start(MoralisSettings.MoralisData.DappUrl, MoralisSettings.MoralisData.DappId);
            Debug.Log("Client Initialized in Moralis.Start()");
        }
        var user = await Moralis.LogInAsync("sumit", "neolen");
        if (user != null && playerUserName != null && playerPassword != null)
        {
            PhotonNetwork.NickName = playerUserName.text;
            PhotonNetwork.ConnectUsingSettings();
        }
    }
    #endregion
 
    #region Photon Callback Methods

    public override void OnConnected()
    {
        Debug.Log("OnConnected is called. The server is available.");
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to Master Server, with the nickname as: " + PhotonNetwork.NickName);
        PhotonNetwork.LoadLevel("SpawnRoom");
    }
    #endregion
}

