using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Realtime;
using Photon.Pun;

public class RoomListing : MonoBehaviour
{
    [SerializeField]
    private TMP_Text roomName;

    [SerializeField]
    private TMP_Text wagerAmount;
    public RoomInfo RoomInfo { get; private set; }

    public void SetRoomInfo(RoomInfo roomInfo)
    {
        RoomInfo = roomInfo;
        roomName.text = roomInfo.Name;
        wagerAmount.text = "Amount: " + roomInfo.CustomProperties["w"].ToString();
    }
    
    public void onClickRoom()
    {
        SceneDataPasser.roomName = this.roomName.text;
        
        PhotonNetwork.LoadLevel("MainScene");
    }
}
