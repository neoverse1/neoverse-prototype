using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
public class EnterChessGame : MonoBehaviour
{
   
    private bool isInTheCollider = false;

    [SerializeField] private GameObject UICanvas;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            UICanvas.SetActive(true);
            isInTheCollider = true;
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        UICanvas.SetActive(false);
        isInTheCollider = false;
    }

    public void ChangeScene()
    {
        if (isInTheCollider)
        {
            PhotonNetwork.LoadLevel("MainScene");
           // SceneManager.LoadScene("MainScene");
        }
    }
}
