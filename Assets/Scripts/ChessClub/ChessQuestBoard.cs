using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
public class ChessQuestBoard : MonoBehaviourPunCallbacks
{
    private bool insideCollider = false;

    private TypedLobby customLobby = new TypedLobby("ChessClubLobby", LobbyType.Default);

    private Dictionary<string, RoomInfo> cachedRoomList = new Dictionary<string, RoomInfo>();

    [SerializeField]
    private GameObject startGameButton;
    
    [SerializeField]
    private GameObject questBoardUI;

    public bool joinRoom = true;

    [SerializeField]
    private TMP_InputField inputField;

    [SerializeField]
    private TMP_InputField wagerInputField;

    public static string wagerAmount;

    private string roomName;

    public bool isRoomPrivate = false;

    [SerializeField]
    private GameObject movementUI;

    [SerializeField]
    private Transform roomListContainer;

    [SerializeField]
    private RoomListing roomListing;

    private List<RoomListing> _listings = new List<RoomListing>();


    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            insideCollider = true;
            startGameButton.SetActive(true);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            insideCollider = false;
            startGameButton.SetActive(false);
        }
    }

    private void OpenRoomListUI()
    {
        //show quest board ui
        joinRoom = false;
        if (PhotonNetwork.InRoom)
            PhotonNetwork.LeaveRoom(this);

       
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();

    }
    
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("connected, called from questboard script");
        if(!joinRoom)
        {
            PhotonNetwork.JoinLobby(TypedLobby.Default);
        }
    }
    private void ShowListofRooms()
    {
        //show room list panel

    }
    /*
    private void UpdateCachedRoomList(List<RoomInfo> roomList)
    {
        for(int i = 0; i < roomList.Count; i++)
        {
            RoomInfo info = roomList[i];

            if (info.RemovedFromList)
                cachedRoomList.Remove(info.Name);
            else
                cachedRoomList[info.Name] = info;
        }
    }*/

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Debug.Log("Joined a lobby");
        cachedRoomList.Clear();
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);

        foreach(RoomInfo info in roomList)
        {
            if(info.RemovedFromList)
            {
                int index = _listings.FindIndex(x => x.RoomInfo.Name == info.Name);
                if(index != -1)
                {
                    Destroy(_listings[index].gameObject);
                    _listings.RemoveAt(index);
                }
            }
            else
            {
                RoomListing listing = Instantiate(roomListing, roomListContainer);
                Debug.Log("Room List Object instaniated");
                if (listing != null)
                {
                    listing.SetRoomInfo(info);
                    _listings.Add(listing);
                }
                   
            }
        }
       // UpdateCachedRoomList(roomList);
    }

    public override void OnLeftLobby()
    {
        base.OnLeftLobby();
        cachedRoomList.Clear();
    }

    #region Button Callback Functions

  //
    public void StartGameButtonClicked()
    {
        movementUI.SetActive(false);
        questBoardUI.SetActive(true);
        OpenRoomListUI();
    }

    public void ExitButtonClicked()
    {
        questBoardUI.SetActive(false);
        movementUI.SetActive(true);
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 10;
        roomOptions.IsVisible = false;
        roomOptions.IsOpen = true;
        PhotonNetwork.JoinOrCreateRoom("ChessClub", roomOptions, TypedLobby.Default);
    }

    public void JoinButtonClicked()
    {
        if(inputField.text!=null)
        {
            PhotonNetwork.JoinRoom(inputField.text);
        }
    }



    #endregion




    #region Create Room Functions

    public void CreateRoom()
    {

        SceneDataPasser.WagerAmount = wagerInputField.text;
        SceneDataPasser.isPrivateGame = isRoomPrivate;
        PhotonNetwork.LoadLevel("MainScene");
       
        /*RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 2;
        roomOptions.IsVisible = !isRoomPrivate;
        roomOptions.IsOpen = true;
        ExitGames.Client.Photon.Hashtable custRoomProps = new ExitGames.Client.Photon.Hashtable();
        custRoomProps.Add("w", wagerInputField.text);
        roomOptions.CustomRoomProperties = custRoomProps;
        string[] properties = new string[] { "w" };
        roomOptions.CustomRoomPropertiesForLobby = properties;
        PhotonNetwork.CreateRoom("Chess Game Room", roomOptions, TypedLobby.Default);
        PhotonNetwork.CurrentRoom.SetPropertiesListedInLobby(properties);*/
    }



    #endregion
}
